use criterion::{criterion_group, criterion_main, Criterion};
#[path = "../src/transform.rs"] mod transform;

fn benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("transform");

    group.bench_function("small transform", |b| b.iter(|| {
        transform::transform_expression("(3-1)-4");
    }));

    group.bench_function("long parse", |b| b.iter(|| {
        transform::transform_expression("4+9-4-3-4-5*9-(3+4)");
    }));

    group.finish();
}

criterion_group!{
    name = benches;
    config = Criterion::default().without_plots();
    targets = benchmark
}

criterion_main!(benches);
