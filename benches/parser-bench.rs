use criterion::{criterion_group, criterion_main, Criterion};
#[path = "../src/parser.rs"] mod parser;

fn benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("parser");

    group.bench_function("small parse", |b| b.iter(|| {
        parser::parse("4+4").unwrap();
    }));

    group.bench_function("long parse", |b| b.iter(|| {
        parser::parse("4+4*8*(4-3*(3+9))").unwrap();
    }));

    group.finish();
}

criterion_group!{
    name = benches;
    config = Criterion::default().without_plots();
    targets = benchmark
}

criterion_main!(benches);
