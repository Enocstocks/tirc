#include <stdlib.h>
#include <stdio.h>
#include "libbf/libbf.h"
#include "src/bigint-utils.h"
void * realloc_big_int(void *, void *, size_t);

extern bf_context_t global_context;

int main(void)
{
  setup();
  bf_t remainder, quotient, value1, value2;
  bf_init(&global_context, &remainder);
  bf_init(&global_context, &quotient);
  bf_init(&global_context, &value1);
  bf_init(&global_context, &value2);

  bf_set_si(&value1, 4);
  bf_set_si(&value2, 2);
  bf_divrem(&quotient, &remainder, &value1, &value2, BF_PREC_INF, BF_RNDZ, BF_RNDZ);

  printf("q: %s\n", string_from_bigint(&quotient, 10));
  printf("r: %s\n", string_from_bigint(&remainder, 10));
  printf("v1: %s\n", string_from_bigint(&value1, 10));
  printf("v2: %s\n", string_from_bigint(&value2, 10));
}
