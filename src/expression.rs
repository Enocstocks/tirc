/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;
use crate::operand;
use crate::functions;
use crate::bigint;

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Expression {
    pub operator: char,
    pub lvalue: Value,
    pub rvalue: Value
}

#[derive(Debug)]
#[derive(PartialEq)]
pub enum Value {
    Exp(Box<Expression>),
    Operand(operand::Operand),
    None,
}

impl Expression {
    pub fn new() -> Expression {
        return Expression {
            operator: '+',
            lvalue: Value::None,
            rvalue: Value::None
        };
    }

// Stopgap to solve mismatch error in parse.rs when a value to any side of the
// tree must be added.
// This should be fixed with generics
    pub fn set_lvalue_from_i64(&mut self, n: i64) {
        self.lvalue = Value::Operand(operand::Operand::Int(n));
    }

    pub fn set_rvalue_from_i64(&mut self, n: i64) {
        self.rvalue = Value::Operand(operand::Operand::Int(n));
    }

    pub fn set_lvalue_from_box(&mut self, n: Box<Expression>) {
        self.lvalue = Value::Exp(n);
    }

    pub fn set_rvalue_from_box(&mut self, n: Box<Expression>) {
        self.rvalue = Value::Exp(n);
    }

    pub fn set_lvalue_from_bigint(&mut self, n: &'static mut bigint::big_int) {
        self.lvalue = Value::Operand(operand::Operand::BigInt(n));
        }

    pub fn set_rvalue_from_bigint(&mut self, n: &'static mut bigint::big_int) {
        self.rvalue = Value::Operand(operand::Operand::BigInt(n));
    }

    pub fn compute(&self, function_map: &HashMap<String, functions::Function>)
                   -> operand::Operand {
        let mut result = bigint::big_int::new();

        match &self.lvalue {
            Value::Operand(x) => {
                match x {
                    operand::Operand::Int(y) => bigint::set_bigint_i64(result, *y),

                    operand::Operand::BigInt(y) => {
                        bigint::set(result, std::ptr::addr_of!(**y) as
                                    *mut bigint::big_int);
                    },

                    _ => bigint::set_bigint_i64(result, 0),
                }
            },
            Value::None => bigint::set_bigint_i64(result, 0),
            Value::Exp(exp) => {
                match exp.compute(function_map) {
                    operand::Operand::Int(y) => bigint::set_bigint_i64(result, y),
                    operand::Operand::BigInt(y) => {
                        bigint::set(result, y);
                    },
                    _ => bigint::set_bigint_i64(result, 0),
                }
            },
        };

        let mut right = match &self.rvalue {
            Value::Operand(x) => {
                match x {
                    operand::Operand::Int(y) => bigint::big_int::from_i64(*y),
                    operand::Operand::BigInt(y) => std::ptr::addr_of!(**y),
                    _ => bigint::big_int::from_i64(0),
                }
            },
            Value::None => bigint::big_int::from_i64(0),
            Value::Exp(exp) => {
                match exp.compute(function_map) {
                    operand::Operand::Int(y) => bigint::big_int::from_i64(y),
                    operand::Operand::BigInt(y) => y,
                    _ => bigint::big_int::from_i64(0),
                }
            },
        };

        match function_map.get(&self.operator.to_string()) {
            Some(x) => {
                match x.function {
                    functions::FunctionType::OneOperand(y) =>
                        y(result),
                    functions::FunctionType::TwoOperand(y) =>
                        y(result, right as *mut bigint::big_int),
                }
            },
            None => bigint::set_bigint_i64(result, 0),
        };

        return operand::Operand::BigInt(result);
    }

    pub fn print_result(&self, function_map: &HashMap<String, functions::Function>) {
        let result = self.compute(function_map);
        result.print_result();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::transform::transform_expression;
    use crate::parser;
    use crate::bigint;
    use std::borrow::BorrowMut;
    use std::collections::HashMap;
    use crate::functions;

    #[test]
    fn new_simple() {
        let s = Expression::new();
        assert_eq!('+', s.operator);
    }

    #[test]
    fn compute() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("4+(5*8)");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };


        assert_eq!("44", tmp);
    }

    #[test]
    fn compute2() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("5*8*(3+2)");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("200", tmp);
    }

    #[test]
    fn compute_with_operation_with_sub() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("3-4+5");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("4", tmp);
    }

    #[test]
    fn compute_with_operation_with_sub_mul() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("25-7*5");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("-10", tmp);
    }

    #[test]
    fn single_negative_number() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("-5");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("-5", tmp);
    }

    #[test]
    fn single_number_with_plus_sign() {
        unsafe { bigint::setup() };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("+5");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("5", tmp);
    }

    #[test]
    fn expression_w_nested_parenthesis() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("((4+9))");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("13", tmp);
    }

    #[test]
    fn expression_w_compound_paren() {
        unsafe { bigint::setup(); };
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("((-3)+5)");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("2", tmp);
    }

    #[test]
    fn expression_w_nums_in_other_numerical_system() {
        let mut hash_function: HashMap<String, functions::Function> = HashMap::new();
        functions::initialize_function_hash(hash_function.borrow_mut());
        let t = transform_expression("0b1111+5+0xa");
        let e = parser::parse(&t);

        let tmp = match e.unwrap().compute(&hash_function) {
            operand::Operand::BigInt(x) => bigint::to_str(x),
            _ => "0",
        };

        assert_eq!("30", tmp);
    }

    #[test]
    fn bad_expression() {
        if let Err(e) = parser::parse("error") {
            assert_eq!("error", e)
        }
    }
}
