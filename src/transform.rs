/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

#[path = "syntax/syntax-predicates.rs"] mod predicates;

// Transform ambiguous expressions to an easy to parse expression.
// Some of the transformations are:
// - Enclose negative numbers in parenthesis
// - Convert a subtraction to a sum of a negative number

// Would it be better/simpler to transform it to reverse polish notation?
pub fn transform_expression(exp: &str) -> String {
    let mut char_set = exp.chars().peekable();
    let mut buffer = String::with_capacity(1024);
    let mut character: char;
    let mut next_char: Option<char>;

    loop {
        next_char = char_set.next();

        if next_char.is_none() {
            break;
        }

        character = next_char.unwrap();

        if character == '-' {
            character = char_set.next().unwrap();

            if character == '(' {
                buffer.push('-');
                buffer.push('(');
                continue;

            } else {
                buffer.push('+');
                buffer.push('(');
                buffer.push('-');
                buffer.push(character);

                loop {
                    let peek = char_set.peek();

                    if peek.is_none() || predicates::operatorp(*peek.unwrap()) {
                        break;
                    }

                    buffer.push(char_set.next().unwrap());
                }

                buffer.push(')');
                continue;
            }
        }

        buffer.push(character);
    }

    return buffer;
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn transformation() {
        assert_eq!("4+(-3)", transform_expression("4-3"));
    }

    #[test]
    fn transformation_w_paren() {
        assert_eq!("7-(4+(-5))", transform_expression("7-(4-5)"));
    }

    #[test]
    fn one_negative_operand() {
        assert_eq!("+(-3)", transform_expression("-3"))
    }

    #[test]
    fn long_paren_expression() {
        assert_eq!("4+(-4)+(-6)+(-7)-(4+(5+(-9)))",
                   transform_expression("4-4-6-7-(4+(5-9))"))
    }

    #[test]
    fn nested_paren_expression() {
        assert_eq!("((((+(-4)))))", transform_expression("((((-4))))"))
    }
}
