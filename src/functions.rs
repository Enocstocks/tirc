/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;
use crate::bigint;

#[derive(Debug)]
pub enum FunctionType {
    OneOperand(fn(*mut bigint::big_int)),
    TwoOperand(fn(*mut bigint::big_int, *mut bigint::big_int)),
}

#[derive(Debug)]
pub struct Function {
    pub function: FunctionType,
}

impl Function {
    pub fn new(function: fn(*mut bigint::big_int)) -> Function {
        return Function { function: FunctionType::OneOperand(function) };
    }

    pub fn new_with_two_operands(function: fn(*mut bigint::big_int, *mut bigint::big_int)) -> Function {
        return Function { function: FunctionType::TwoOperand(function) };
    }
}

#[inline(always)]
pub fn initialize_function_hash(hash: &mut HashMap<String, Function>) {
    // Add tirc function primitives
    hash.insert("+".to_string(),
                Function::new_with_two_operands(bigint::add));
    hash.insert("-".to_string(),
                Function::new_with_two_operands(bigint::sub));
    hash.insert("*".to_string(),
                Function::new_with_two_operands(bigint::multiplication));
    hash.insert("/".to_string(),
                Function::new_with_two_operands(bigint::division));
    hash.insert("^".to_string(),
                Function::new_with_two_operands(bigint::power));
}
