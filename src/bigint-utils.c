/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include "../libbf/libbf.h"
#include "bigint-utils.h"

bf_context_t global_context;

/*
  Populates the fields in global_context with default values to any new big int.

  global_context do not need complex values since the usage of libbf is kinda
  simple.
*/
void setup(void)
{
  bf_context_init(&global_context, realloc_big_int, NULL);
}

/*
  libbf's requires a realloc function to make and resize big ints, this realloc
  function is just a libc's realloc wrapper. Since the usage of libbf in tirc is
  simple I do not need a complex realloc, at least for now.
*/
void * realloc_big_int(void * data, void * ptr, size_t size)
{
  void * result = realloc(ptr, size);

  if (result == NULL) {
#ifdef DEBUG
    printf("Realloc error\n");
#endif
    result = ptr;
  }

  return result;
}

void print_bigint(bf_t * bigint, int radix)
{
  printf("%s\n", string_from_bigint(bigint, radix));
}

char * string_from_bigint(bf_t * bigint, int radix)
{
  char * result = bf_ftoa(NULL, bigint, radix, 0, BF_RNDZ | BF_FTOA_FORMAT_FRAC);
  if (result == NULL)
    printf("Error in bigint_from_string::bf_ftof\n");

  return result;
}

void bigint_from_string(bf_t * destination, char * value, int radix)
{
  int result = bf_atof(destination, value, NULL, radix, BF_PREC_INF, BF_RNDZ);

  if (result & BF_ST_MEM_ERROR)
    printf("bigint_from_string: Memory error\n");
}

int is_nan(bf_t * bigint)
{
  if (bigint->expn == BF_EXP_NAN && bigint->len == 0)
    return 1;
  else
    return 0;
}

void * malloc_bigint(void)
{
  void * result =  malloc(sizeof(bf_t));
  if (result == NULL)
    printf("Error in malloc\n");

  return result;
}

void bigint_add_i64(bf_t * bigint, int64_t i64)
{
  bf_add_si(bigint, bigint, i64, BF_PREC_INF, BF_RNDZ);
}

void bigint_add(bf_t * dest, bf_t * to_add)
{
  bf_add(dest, dest, to_add, BF_PREC_INF, BF_RNDZ);
}

void bigint_multiplication(bf_t * dest, bf_t * multiplier)
{
  bf_mul(dest, dest, multiplier, BF_PREC_INF, BF_RNDZ);
}

// The result of the operation is stored in minuend
void bigint_sub(bf_t * minuend, bf_t * subtrahend)
{
  bf_sub(minuend, minuend, subtrahend, BF_PREC_INF, BF_RNDZ);
}

void bigint_set(bf_t * dest, bf_t * source)
{
  int result = bf_set(dest, source);

  if (result == BF_ST_MEM_ERROR)
    printf("Error in bigint_set\n");
}

void print_result(bf_t * bigint)
{
  print_bigint(bigint, 10);
  printf("Hexadecimal: ");
  print_bigint(bigint, 16);
  printf("Octal:       ");
  print_bigint(bigint, 8);
  printf("Binary:      ");
  print_bigint(bigint, 2);
  bf_delete(bigint);
}

void bigint_power(bf_t * base, bf_t * exponent)
{
  int64_t exp;
  bf_t tmp;
  bf_init(&global_context, &tmp);

  bf_get_int64(&exp, exponent, BF_RNDZ);
  bf_pow_ui(&tmp, base, exp, BF_PREC_INF, BF_RNDZ);
  bf_set(base, &tmp);
  bigint_drop(&tmp);
}

void bigint_division(bf_t * dividend, bf_t * divisor)
{
  bf_t quotient, remainder;
  bf_init(&global_context, &quotient);
  bf_init(&global_context, &remainder);

  bf_divrem(&quotient, &remainder, dividend, divisor, BF_PREC_INF, BF_RNDZ,
            BF_RNDZ);

  // set the first argument with the division result
  bf_set(dividend, &quotient);

  bigint_drop(&quotient);
  bigint_drop(&remainder);
}

void teardown(void)
{
  bf_context_end(&global_context);
}

void bigint_drop(bf_t * bigint)
{
  bf_delete(bigint);
}
