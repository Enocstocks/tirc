/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

#![allow(dead_code)]

pub fn operatorp(character: char) -> bool {
    if character == '+' || character == '-' || character == '*' ||
        character == '/' || character == '^'
    {
        return true;
    }

    return false;
}

pub fn compoundp(expression: &str) -> bool {
    let mut char_set = expression.chars();
    let mut count: i64 = 0;

    loop {
        let next = char_set.next();

        if next.is_none() {
            return false;
        }

        let character = next.unwrap();

        if character == '(' {
            count += 1;
        }

        if character == ')' {
            count -= 1;
        }

        if operatorp(character) && count == 0 {
            return true;
        }
    }
}

pub fn operator_1st_precedencep(character: char) -> bool {
    if character == '^' {
        return true;
    }

    return false;
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn operatorp_true() {
        assert!(operatorp('+'));
    }

    #[test]
    fn operatorp_false() {
        assert!(!operatorp('3'));
    }

    #[test]
    fn compoundp_true() {
        assert!(compoundp("54+3"));
    }

    #[test]
    fn compoundp_false() {
        assert!(!compoundp("543"));
    }

    #[test]
    fn operator_1st_precedencep_true() {
        assert!(operator_1st_precedencep('^'));
    }

    #[test]
    fn operator_1st_precedencep_false() {
        assert!(!operator_1st_precedencep('*'));
    }
}
