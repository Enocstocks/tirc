/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

#![allow(non_camel_case_types)]

use std::ffi::{c_void, c_char, c_int, CString, CStr};

#[link(name = "bf", kind = "static")]
extern "C" {
    pub static mut global_context: big_int;
    pub fn setup();
    pub fn teardown();
    pub fn bf_init(context: *mut c_void, bigint: *mut big_int);
    pub fn bf_set_si(destination: *mut big_int, value: i64);
    pub fn print_bigint(integer: *mut big_int, radix: i32);
    pub fn print_result(integer: *mut big_int);
    pub fn bigint_from_string(destination: *mut big_int, string: *const u8, radix: i64);
    pub fn bigint_add_i64(bigint: *mut big_int, value: i64);
    pub fn bigint_add(bigint0: *mut big_int, bigint1: *mut big_int);
    pub fn bigint_sub(minuend: *mut big_int, subtrahend: *mut big_int);
    pub fn bigint_power(base: *mut big_int, exponent: *mut big_int);
    pub fn bigint_division(dividend: *mut big_int, divisor: *mut big_int);
    pub fn bigint_multiplication(multiplicand: *mut big_int, multiplier: *mut big_int);
    pub fn bigint_set(bigint0: *mut big_int, bigint1: *mut big_int);
    pub fn string_from_bigint(destination: *mut big_int, radix: i32) -> *const c_char;
    pub fn is_nan(bigint: *mut big_int) -> c_int;
    pub fn malloc_bigint() -> &'static mut big_int;
    pub fn bigint_drop(bigint: *mut big_int);
}

// Rough representation of bf_t, it will not work in non-64 bits architectures
// due to [iu]64 fields. This struct is unused and its only purpose is to allow
// the representation of ~c_void pointer~ when debugging

#[derive(Debug, PartialEq)]
#[repr(C)]
pub struct big_int {
    pub ctx: *mut c_void,
    pub sign: std::os::raw::c_int,
    pub expn: i64,
    pub len: u64,
    pub tab: *mut u64,
}

impl big_int {
    pub fn new() -> &'static mut big_int {
        let tmp: &mut big_int = unsafe { malloc_bigint() };

        unsafe { bf_init(std::ptr::addr_of!(global_context) as *mut c_void,
                         tmp); }
        return tmp;
    }

    pub fn from_i64(value: i64) -> &'static mut big_int {
        let tmp = big_int::new();
        set_bigint_i64(tmp, value);
        return tmp;
    }

    pub fn new_from_str(string: &str, radix: i64) -> &'static mut big_int {
        let cstring = CString::new(string);
        let bigint = big_int::new();

        if let Err(_) = cstring {
            panic!("Null found within {}", string);
        };

        unsafe {
            bigint_from_string(bigint, cstring.unwrap().as_ptr() as *const u8, radix);
        }

        return bigint;
    }
}

pub fn is_bigint_nan(bigint: *mut big_int) -> c_int {
    return unsafe { is_nan(bigint) };
}

pub fn set_bigint_i64(bigint: *mut big_int, value: i64) {
    unsafe { bf_set_si(bigint, value)  };
}

pub fn add_i64(bigint: *mut big_int, value: i64) {
    unsafe { bigint_add_i64(bigint, value) };
}

pub fn add(bigint0: *mut big_int, bigint1: *mut big_int) {
    unsafe { bigint_add(bigint0, bigint1) };
    unsafe { bigint_drop(bigint1) };
}

// the result is stored in minuend
pub fn sub(minuend: *mut big_int, subtrahend: *mut big_int) {
    unsafe { bigint_sub(minuend, subtrahend) };
    unsafe { bigint_drop(subtrahend) };
}

// the multiplication result is stored in multiplicand
pub fn multiplication(multiplicand: *mut big_int, multiplier: *mut big_int) {
    unsafe { bigint_multiplication(multiplicand, multiplier) };
    unsafe { bigint_drop(multiplier) };
}

pub fn set(bigint0: *mut big_int, bigint1: *mut big_int) {
    unsafe { bigint_set(bigint0, bigint1) };
    unsafe { bigint_drop(bigint1) };
}

pub fn power(base: *mut big_int, exponent: *mut big_int) {
    unsafe { bigint_power(base, exponent) };
    unsafe { bigint_drop(exponent) };
}

pub fn division(dividend: *mut big_int, divisor: *mut big_int) {
    unsafe { bigint_division(dividend, divisor) };
    unsafe { bigint_drop(divisor) };
}

pub fn to_str(bigint: *mut big_int) -> &'static str {
    let tmp = unsafe { string_from_bigint(bigint, 10) };
    let result: &CStr = unsafe { CStr::from_ptr(tmp) };
    return result.to_str().unwrap();
}

pub fn print_in_all_systems(bigint: *mut big_int) {
    unsafe {
        print_result(bigint);
    };
}

pub fn drop(bigint: *mut big_int) {
    unsafe { bigint_drop(bigint) };
}

#[cfg(test)]
mod test {
    use super::*;
    use std::ffi::CStr;

    #[test]
    fn set_small_bigint() {
        unsafe { setup(); };
        let bigint = big_int::new();
        unsafe { bf_set_si(bigint, 32); };
        let result: &CStr =
            unsafe { CStr::from_ptr(string_from_bigint(bigint, 10)) };
        assert_eq!("32", result.to_str().unwrap());
    }

    #[test]
    fn from_str_giant_bigint() {
        unsafe { setup(); };
        let bigint = big_int::new_from_str(
            "3280411005000984487577096338671603500228214583512268800000000000",
            10
        );

        let result: &CStr =
            unsafe { CStr::from_ptr(string_from_bigint(bigint, 10)) };
        assert_eq!(
            "3280411005000984487577096338671603500228214583512268800000000000",
            result.to_str().unwrap());
    }

    #[test]
    fn is_nan_false() {
        unsafe { setup(); };
        let tmp = big_int::new();
        set_bigint_i64(tmp, 20);
        assert_eq!(0, is_bigint_nan(tmp));
    }

    #[test]
    fn is_nan_false_from_str() {
        unsafe { setup(); };
        let tmp = big_int::new_from_str(
            "3280411005000984487577096338671603500228214583512268800000000000",
            10
        );
        assert_eq!(0, is_bigint_nan(tmp));
    }

    #[test]
    fn is_nan_true() {
        unsafe { setup(); };
        let tmp = big_int::new_from_str("a", 10);
        assert_eq!(1, is_bigint_nan(tmp));
    }

    #[test]
    fn add_i64_to_a_bigint() {
        unsafe { setup(); };
        let mut bigint = big_int::new_from_str("190", 10);
        add_i64(bigint, 5);
        let result = unsafe { CStr::from_ptr(string_from_bigint(bigint, 10)) };
        assert_eq!("195", result.to_str().unwrap());
    }

    #[test]
    fn add_bigints() {
        unsafe { setup(); };
        let mut bigint0 = big_int::new_from_str("190", 10);
        let mut bigint1 = big_int::new_from_str("110", 10);
        add(bigint0, bigint1);
        let result = unsafe { CStr::from_ptr(string_from_bigint(bigint0, 10)) };
        assert_eq!("300", result.to_str().unwrap());
    }

    #[test]
    fn set_bigint(){
        unsafe { setup(); };
        let mut bigint0 = big_int::new_from_str("220", 10);
        let mut bigint1 = big_int::new();
        set(bigint1, bigint0);
        let result = unsafe { CStr::from_ptr(string_from_bigint(bigint1, 10)) };
        assert_eq!("220", result.to_str().unwrap())
    }

    #[test]
    fn power_bigints() {
        unsafe { setup(); };
        let mut bigint0 = big_int::new_from_str("3", 10);
        let mut bigint1 = big_int::new_from_str("4", 10);
        power(bigint0, bigint1);
        let result = unsafe { CStr::from_ptr(string_from_bigint(bigint0, 10)) };
        assert_eq!("81", result.to_str().unwrap());
    }

    #[test]
    fn sub_bigints() {
        unsafe { setup(); };
        let mut bigint0 = big_int::new_from_str("21", 10);
        let mut bigint1 = big_int::new_from_str("7", 10);
        sub(bigint0, bigint1);
        let result = unsafe { CStr::from_ptr(string_from_bigint(bigint0, 10)) };
        assert_eq!("14", result.to_str().unwrap());
    }

    #[test]
    fn div_bigints() {
        unsafe { setup(); };
        let mut bigint0 = big_int::new_from_str("21", 10);
        let mut bigint1 = big_int::new_from_str("7", 10);
        division(bigint0, bigint1);
        let result = unsafe { CStr::from_ptr(string_from_bigint(bigint0, 10)) };
        assert_eq!("3", result.to_str().unwrap());
    }
}
