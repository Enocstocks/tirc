/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

use clap::{ App, Arg };

pub fn parse_args() -> App<'static, 'static> {
    return App::new("tirc")
        .author("Bryan H. <masterenoc@tutamail.com>")
        .about("Tiny calculator made in rust")
        .long_about(
"Tiny calculator with support of basic arithmetic, It
uses binary trees to store and calculate mathematical
expressions")
        .version("0.1.0")
        .arg(Arg::with_name("debug")
             .short("d")
             .long("debug")
             .help("Enable debug mode")
        )
        .arg(Arg::with_name("expression")
             .index(1)
        );
}
