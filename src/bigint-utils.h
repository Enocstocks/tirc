/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

void setup(void);
void * realloc_big_int(void *, void *, size_t);
void print_bigint_decimal(bf_t *, int);
char * string_from_bigint(bf_t *, int);
void bigint_from_string(bf_t *, char *, int);
int is_nan(bf_t * bigint);
void * malloc_bigint();
void bigint_add_i64(bf_t *, int64_t);
void bigint_add(bf_t *, bf_t *);
void bigint_set(bf_t *, bf_t *);
void bigint_sub(bf_t *, bf_t *);
void bigint_multiplication(bf_t *, bf_t *);
void print_result(bf_t *);
void bigint_division(bf_t *, bf_t *);
void teardown(void);
void bigint_drop(bf_t *);
void bigint_power(bf_t * base, bf_t * exponent);
