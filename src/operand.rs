/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

#![allow(dead_code)]
use std::fmt;
use std::str::FromStr;
use crate::bigint;

#[derive(Debug)]
#[derive(PartialEq)]
pub enum Operand {
    BigInt(&'static mut bigint::big_int),
    Int(i64),
    None,
}

impl fmt::Binary for Operand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        let bytes = match self {
            Operand::Int(x) => x.to_be_bytes(),
            _ => 0_i64.to_be_bytes(),
        };

        return write!(f, "{:b} {:b} {:b} {:b} {:b} {:b} {:b} {:b}",
                      bytes[0], bytes[1], bytes[2], bytes[3], bytes[4],
                      bytes[5], bytes[6], bytes[7]);
    }
}

impl Operand {
    pub fn new(n: i64) -> Operand {
        return Operand::Int(n);
    }

    pub fn print_result(&self) {
        if let Operand::BigInt(num) = self {
            bigint::print_in_all_systems(std::ptr::addr_of!(**num) as
                                         *mut bigint::big_int);
        }
    }

    pub fn from_str(s: &str) -> Result<Operand, &str> {
        let len = s.chars().count();
        let num_system_mark = s.get(0..2);
        let num: i64;
        let bigint: &mut bigint::big_int;

        if num_system_mark.is_none() {
            match i64::from_str(s) {
                Ok(int) => return Ok(Operand::Int(int)),
                Err(_) => {
                    return Err(s);
                },
            }
        }

        let new_string = s.get(2..len).unwrap();

        match num_system_mark.unwrap() {
            "0x" => {
                if let Ok(i) = i64::from_str_radix(new_string, 16) {
                    num = i;
                } else {
                    bigint = bigint::big_int::new_from_str(s, 16);

                    if bigint::is_bigint_nan(bigint) == 1 {
                        return Err(new_string);
                    }

                    return Ok(Operand::BigInt(bigint));
                }
            },
            "0b" => {
                if let Ok(i) = i64::from_str_radix(new_string, 2) {
                    num = i;
                } else {
                    bigint = bigint::big_int::new_from_str(s, 2);

                    if bigint::is_bigint_nan(bigint) == 1 {
                        return Err(new_string);
                    }

                    return Ok(Operand::BigInt(bigint));
                }
            },
            "0o" => {
                if let Ok(i) = i64::from_str_radix(new_string, 8) {
                    num = i;
                } else {
                    bigint = bigint::big_int::new_from_str(s, 8);

                    if bigint::is_bigint_nan(bigint) == 1 {
                        return Err(new_string);
                    }

                    return Ok(Operand::BigInt(bigint));
                }
            },
            _ => {
                if let Ok(i) = i64::from_str_radix(s, 10) {
                    num = i;
                } else {
                    bigint = bigint::big_int::new_from_str(s, 10);

                    if bigint::is_bigint_nan(bigint) == 1 {
                        return Err(s);
                    }

                    return Ok(Operand::BigInt(bigint));
                }
            }
        }

        return Ok(Operand::Int(num));
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ffi::CStr;
    use crate::bigint::string_from_bigint;

    #[test]
    fn binary_format() {
        assert_eq!(format!("{:b}", Operand::new(999)), "0 0 0 0 0 0 11 11100111");
    }

    #[test]
    fn from_str() {
        let tmp = match Operand::from_str("199").unwrap() {
            Operand::Int(x) => x,
            _ => 0,
        };

        assert_eq!(199, tmp);
    }

    #[test]
    fn from_str_err() {
        if let Err(i) = Operand::from_str("q") {
            assert_eq!("q".to_string(), i.to_string())
        }
    }

    #[test]
    fn from_binary() {
        let tmp = match Operand::from_str("0b1000").unwrap() {
            Operand::Int(x) => x,
            _ => 0,
        };

        assert_eq!(8, tmp);
    }

    #[test]
    fn from_octal() {
        let tmp = match Operand::from_str("0o777").unwrap() {
            Operand::Int(x) => x,
            _ => 0,
        };

        assert_eq!(511, tmp);
    }

    #[test]
    fn from_hex() {
         let tmp = match Operand::from_str("0x1a5").unwrap() {
            Operand::Int(x) => x,
            _ => 0,
        };

        assert_eq!(421, tmp);
    }

    #[test]
    fn bad_operand_from_str() {
        if let Err(e) = Operand::from_str("f") {
            assert_eq!("f", e)
        }
    }

    #[test]
    fn bad_operand_from_str_long_token() {
        if let Err(e) = Operand::from_str("error") {
            assert_eq!("error", e)
        }
    }

    #[test]
    fn get_bigint_from_str() {
        let bigint = Operand::from_str(
            "32804110050009844875770963386716035002282145835122688");

        let result = match bigint {
                Ok(Operand::BigInt(x)) => {
                    let tmp: &CStr =
                        unsafe { CStr::from_ptr(string_from_bigint(x, 10)) };

                    tmp.to_str().unwrap()
                },
                _ => "0",
        };

        assert_eq!(
            "32804110050009844875770963386716035002282145835122688",
            result);
    }
}
