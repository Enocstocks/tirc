/*
   This file is part of Tirc.

   Tirc is free software: you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   Tirc is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
   details.

   You should have received a copy of the GNU Affero General Public License along
   with Tirc. If not, see <https://www.gnu.org/licenses/>.
*/

#[path = "syntax/syntax-predicates.rs"] mod predicates;
use crate::operand;
use crate::expression;

pub fn get_next_operator(strg: &str) -> Option<(char, usize)> {
    let mut char_set = strg.char_indices();
    let mut count = 0;
    let mut last_operator: Option<(char, usize)> = None;

    loop {
        let next_char = char_set.next();

        if next_char.is_none() {
            return last_operator;
        }

        let (position, character) = next_char.unwrap();

        if character == '(' {
            count += 1;
        }

        if character == ')' {
            count -= 1;
        }

        if predicates::operatorp(character) && count == 0 {
            if last_operator.is_none() {
                last_operator = Some((character, position));
            }

            if predicates::operator_1st_precedencep(character) {
                return Some((character, position));
            }

            let indice = last_operator.unwrap();

            if operator_precedence(character) > operator_precedence(indice.0) {
                last_operator = Some((character, position));
            }
        }
    }
}

fn operator_precedence(character: char) -> i64 {
    if character == '^' {
        return 1;
    }

    if character == '*' {
        return 2;
    }

    if character == '/' {
        return 2;
    }

    if character == '-' {
        return 3;
    }

    if character == '+' {
        return 3;
    }

    return 0;
}

pub fn get_operand(beg: usize, end: usize, strg: &str) -> &str {
    let operand = strg.get(beg..end).unwrap();
    return remove_operand_parenthesis(operand);
}

// This functions is going to be used in left or right side
// operands so it only has to recursively remove parenthesis
// if they are found at the beginnig of the operand,
// otherwise it is either a single operand or a complex one
// (which is going to be splited into operands until only
// single operands are left)
pub fn remove_operand_parenthesis(string: &str) -> &str {
    let mut chars_set = string.chars();
    let character = chars_set.next();
    let mut unparenthesis_string: &str = string;

    if character.is_some() {
        if predicates::compoundp(string) {
            return unparenthesis_string;
        }

        if character.unwrap() == '(' {
            unparenthesis_string = remove_operand_parenthesis(string.get(1..chars_set.count()).unwrap());
        }
    }

    return unparenthesis_string;
}

pub fn parse(mut expression: &str) -> Result<expression::Expression, &str> {
    let mut operator = get_next_operator(expression);
    let mut len = expression.char_indices().count();
    let mut tree = expression::Expression::new();

    if expression.is_empty() {
        return Err("Empty expression");
    }

    if operator.is_none() {
        let operand = get_operand(0, len, expression);

        if predicates::compoundp(operand) {
            expression = operand;
            operator = get_next_operator(expression);
            len = expression.char_indices().count();
        } else {
            match operand::Operand::from_str(operand) {
                Ok(i) => {
                    match i {
                        operand::Operand::Int(x) => {
                            tree.set_lvalue_from_i64(x)
                        },
                        operand::Operand::BigInt(x) => {
                            tree.set_lvalue_from_bigint(x)
                        },
                        _ => tree.set_lvalue_from_i64(0),
                    };
                },
                Err(e) => {
                    return Err(&e);
                }
            }

            return Ok(tree);
        }
    }

    let (character, position) = operator.unwrap();
    tree.operator = character;

    // Left value calculation
    // Position 0 means a number with a operand in front is given, these cases
    // need to be handled as if 0 is on the left side i. e. -1 becomes 0-1
    if position == 0 {
        tree.set_lvalue_from_i64(0);
    } else {

        let left_expression = get_operand(0, position, expression);

        if predicates::compoundp(&left_expression) {
            tree.set_lvalue_from_box(Box::new(parse(&left_expression).unwrap()));
        } else {
            match operand::Operand::from_str(&left_expression) {
                Ok(i) => {
                    let tmp = match i {
                        operand::Operand::Int(x) => x,
                        _ => 0,
                    };

                    tree.set_lvalue_from_i64(tmp);
                },
                Err(e) => {
                    println!("{}", e);
                    return Err(e);
                }
            }
        }

    }

    // Right value calculation
    let right_expression = get_operand(position+1, len, &expression);

    if predicates::compoundp(&right_expression) {
        tree.set_rvalue_from_box(Box::new(parse(&right_expression).unwrap()));
        return Ok(tree);
    } else {
        match operand::Operand::from_str(&right_expression) {
            Ok(i) => {
                let tmp = match i {
                    operand::Operand::Int(x) => x,
                    _ => 0,
                };

                tree.set_rvalue_from_i64(tmp);
            },
            Err(e) => {
                println!("{}", e);
                return Err(e);
            }
        }

        return Ok(tree);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::bigint;
    use crate::operand;

    #[test]
    fn get_operator_plus() {
        let statement = String::from("(5-4)+4");
        assert_eq!(('+', 5), get_next_operator(&statement).unwrap());
    }

    #[test]
    fn get_operator_minus() {
        let statement = String::from("(5-4)*4-3");
        assert_eq!(('-', 7), get_next_operator(&statement).unwrap());
    }

    #[test]
    fn get_operator_nearest_div() {
        let statement = String::from("3/4*5/9*90");
        assert_eq!(('/', 1), get_next_operator(&statement).unwrap());
    }

    #[test]
    fn get_operator_nearest_minus() {
        let statement = String::from("3-4/17*90");
        assert_eq!(('-', 1), get_next_operator(&statement).unwrap());
    }

    #[test]
    fn get_operator_farthest_plus() {
        let statement = String::from("3/4*5/9+90");
        assert_eq!(('+', 7), get_next_operator(&statement).unwrap());
    }

    #[test]
    fn get_unary_delimiter() {
        let statement = String::from("(5+4)");

        assert!(get_next_operator(&statement).is_none());
    }

    #[test]
    fn get_compound_operand_l() {
        let statement = String::from("(5+4)+4");
        let (_op, end) = get_next_operator(&statement).unwrap();
        let operand = get_operand(0, end, &statement);
        assert_eq!("5+4", operand);
    }

    #[test]
    fn get_compound_operand_r() {
        let statement = String::from("4+(4+8)");
        let (_op, end) = get_next_operator(&statement).unwrap();
        let operand = get_operand(end+1, statement.char_indices().count(), &statement);
        assert_eq!("4+8", operand);
    }

    #[test]
    fn test_get_operand() {
        let statement = String::from("7+4");
        let (_op, end) = get_next_operator(&statement).unwrap();
        let operand = get_operand(0, end, &statement);
        assert_eq!("7", operand);
    }

    #[test]
    fn remove_parenthesis_when_parenthesis() {
        assert_eq!("5+4", remove_operand_parenthesis("(5+4)"));
    }

    #[test]
    fn remove_parenthesis_when_non_parenthesis() {
        assert_eq!("5+4", remove_operand_parenthesis("5+4"));
    }

    #[test]
    fn remove_parenthesis_recursive() {
        assert_eq!("5+4", remove_operand_parenthesis("((5+4))"));
    }

    #[test]
    fn remove_consecutive_parenthesis() {
        assert_eq!("(3)+(4)", remove_operand_parenthesis("(3)+(4)"));
    }

    #[test]
    fn parse_simple() {
        let s = parse("45");

        let l:i64 = match s.unwrap().lvalue {
            expression::Value::Operand(x) => {
                match x {
                    operand::Operand::Int(y) => y,
                    _ => 0,
                }
            },
            _ => 0
        };

        assert_eq!(45, l);
    }

    #[test]
    fn parse_compound() {
        let s = parse("3+4").unwrap();

        let mut tree = expression::Expression::new();
        tree.operator = '+';
        tree.set_lvalue_from_i64(3);
        tree.set_rvalue_from_i64(4);

        assert_eq!(s, tree);
    }

    #[test]
    fn new_compound_more_complex() {
        let statement = "3+4*4-9";
        let s = parse(&statement).unwrap();

        let mut tree = expression::Expression::new();
        tree.operator = '+';
        tree.set_lvalue_from_i64(3);

        let mut tree_2 = expression::Expression::new();
        tree_2.operator = '-';
        tree_2.set_rvalue_from_i64(9);

        let mut tree_3 = expression::Expression::new();
        tree_3.operator = '*';
        tree_3.set_lvalue_from_i64(4);
        tree_3.set_rvalue_from_i64(4);

        tree_2.set_lvalue_from_box(Box::new(tree_3));
        tree.set_rvalue_from_box(Box::new(tree_2));

        assert_eq!(s, tree);
    }

    #[test]
    fn new_compound_w_paren() {
        let s = parse("4+(1-3)").unwrap();

        let mut tree = expression::Expression::new();
        tree.operator = '+';
        tree.set_lvalue_from_i64(4);

        let mut tree_2 = expression::Expression::new();
        tree_2.operator = '-';
        tree_2.set_lvalue_from_i64(1);
        tree_2.set_rvalue_from_i64(3);

        tree.set_rvalue_from_box(Box::new(tree_2));

        assert_eq!(s, tree);
    }

    #[test]
    fn parse_simple_bigint() {
        let result = parse("69900000000000000000000000000000000000000000000").unwrap();

        let mut result_pointer: *mut bigint::big_int = match result.lvalue {
            expression::Value::Operand(x) => match x {
                operand::Operand::BigInt(y) => y,
                _ => std::ptr::null_mut(),
            },
            _ => std::ptr::null_mut(),
        };

        let result_str = unsafe { bigint::string_from_bigint(result_pointer, 10) };

        let bigint = bigint::big_int::new_from_str(
            "69900000000000000000000000000000000000000000000", 10
        );

        let expected = unsafe { bigint::string_from_bigint(bigint, 10) };

        unsafe { assert_eq!(std::ffi::CStr::from_ptr(expected),
                            std::ffi::CStr::from_ptr(result_str)) };
    }
}
