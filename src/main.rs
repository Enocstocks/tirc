 /*
   Copyright 2021-2022  Bryan Hernández <masterenoc@tutamail.com>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::process::exit;
use std::io;
use std::io::{Write};
use clap::{ ArgMatches };
use std::borrow::BorrowMut;
use std::collections::HashMap;

#[path = "./functions.rs"] mod functions;
mod operand;
mod bigint;
mod args;
mod transform;
mod parser;
mod expression;

fn main() {
    let mut debug: bool = false;
    let arguments: ArgMatches = args::parse_args().get_matches();
    let mut functions_hash: HashMap<String, functions::Function> = HashMap::new();

    functions::initialize_function_hash(functions_hash.borrow_mut());
    unsafe { bigint::setup() };

    if arguments.is_present("debug") {
        debug = true;
    }

    // Only parse an expression when the expression is given by command line
    // arguments
    if arguments.is_present("expression") {
        let exp: &str = arguments.value_of("expression").unwrap();
        let transformed_expression: &str = &transform::transform_expression(exp);
        let expression = parser::parse(transformed_expression);

        if let Err(e) = expression {
            print_parse_error(&e);
            exit(1)
        }

        if debug {
            println!("New expression form: {}", transformed_expression);
            println!("* DEBUG STRUCTURE *\n{:#?}\n", expression);
        }

        expression.unwrap().print_result(&functions_hash);
        drop(functions_hash);
        unsafe { bigint::teardown() };
        exit(0);
    }

    // Start REPL when no expression is given
    println!("Starting REPL");

    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut stdout = io::stdout();

    loop {
        buffer.clear();

        print!("> ");

        match stdout.flush() {
            Ok(_) => {},
            Err(e) => {panic!("{}", e)}
        };

        match stdin.read_line(&mut buffer) {
            Ok(_) => {

                buffer = buffer.trim_matches('\n').to_string();
                buffer = buffer.trim_matches('\r').to_string();

                if buffer == "q" {
                    break;
                }

                let transformed_expression = &transform::transform_expression(&buffer);
                let expression = parser::parse(transformed_expression);

                if let Err(e) = expression {
                    print_parse_error(&e);
                    continue
                }

                if debug {
                    println!("New expression form: {}", transformed_expression);
                    println!("* DEBUG STRUCTURE *\n{:#?}\n", expression);
                }

                expression.unwrap().print_result(&functions_hash);
            },

            Err(_) => {
                println!("ERROR: bad input");
            }
        };
    }

    drop(functions_hash);
    unsafe { bigint::teardown() };
}

#[inline(always)]
fn print_parse_error(e: &str) {
    println!("Syntax error: \"{}\" bad token\n", e);
}
