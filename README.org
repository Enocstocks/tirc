* Overview
~tirc~ is a tiny interactive rust calculator. I used to use
~gnome-calculator~ and it worked for me; but, after some time, I
deleted gnome and gnome-calculator was gone. I sought for a CLI
calculator and I found [[http://www.isthe.com/chongo/tech/comp/calc/index.html][calc]], which is an overkill and has much more
features that I need.

~tirc~ is heavily inspired in ~calc~ and for now is simpler and much
more smaller.

* Dependencies
- [[https://www.bellard.org/libbf/][libbf]] (Tested with version 2020-01-19): library for multi-precision
  arithmetic.
- ~Dejagnu~ (optional): for integration testing.

* Features
Currently supports basic arithmetic with any infinite-precision
integers, parse and display numbers with different numerical systems
like binary, octal, decimal and hexadecimal.

* Install
First, Download and extract libbf in the ~libbf~ directory before
start the compilation process, then you can run ~make~ to compile
libbf and all the C files used by ~tirc~.

You can now run ~tirc~ with ~cargo run~ and install it with ~cargo
install~.
