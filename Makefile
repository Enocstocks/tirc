.phony: bench bench-release crit-bench run-bench etags build clean test unit-test \
	integration-test

build: src/libbf.a
	cargo build

src/bigint-utils.o: src/bigint-utils.c
	gcc -c -O0 -ggdb -Wall -Wextra -o src/bigint-utils.o $^

src/libbf.o: libbf/libbf.c libbf/cutils.o
	make -C libbf

src/libbf.a: src/libbf.o src/bigint-utils.o
	ar -rcs src/libbf.a libbf/libbf.o libbf/cutils.o src/bigint-utils.o

bench:
	perf stat -r 1000 -- target/debug/tirc "4+4*8*(4-3*(3+9))" 1> /dev/null

bench-release:
	perf stat -r 1000 -- target/release/tirc "4+4*8*(4-3*(3+9))" 1> /dev/null

crit-bench:
	rustc -o benches/parser_bench --edition=2021 benches/parser-bench.rs -C opt-level=3 \
	-C embed-bitcode=no -L dependency=/home/deepnoc/projects/tirc/target/release/deps \
	--extern clap=/home/deepnoc/projects/tirc/target/release/deps/libclap-93ee464844a31270.rlib \
	--extern criterion=/home/deepnoc/projects/tirc/target/release/deps/libcriterion-934b48eb15ee446a.rlib
	rustc -o benches/transform_bench --edition=2021 benches/transform-bench.rs -C opt-level=3 \
	-C embed-bitcode=no -L dependency=/home/deepnoc/projects/tirc/target/release/deps \
	--extern clap=/home/deepnoc/projects/tirc/target/release/deps/libclap-93ee464844a31270.rlib \
	--extern criterion=/home/deepnoc/projects/tirc/target/release/deps/libcriterion-934b48eb15ee446a.rlib

run-bench:
	cargo-criterion --precompiled-bins benches/parser_bench benches/transform_bench

etags:
	etags -o LIBBF-TAGS libbf/libbf.c libbf/libbf.h

clean:
	rm src/libbf.*

debug:
	rust-gdb target/debug/tirc

c: test.c
	gcc -O0 -ggdb -o test test.c libbf/libbf.o libbf/cutils.o src/bigint-utils.c

unit-test: src/libbf.a
	cargo test

val:
	valgrind --leak-check=yes --leak-check=full target/debug/tirc 4+3

integration-test:
	runtest --srcdir tests/integration --outdir tests/ --tool tirc

test: unit-test integration-test
